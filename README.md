﻿# Sum Of Positives

## Description

### Task 

Build the sum of only the positive values in any given array.

### Input

An array of integer.

### Output

The sum of all positive values in the input array.

## Difficulty: Easy

---

# How To Start

## Creating a new session

1. Create a new branch from the `develop` branch:
   
![Create new branch from develop](doc/how_to_start/create_branch_from_develop.png)
   
2. Name the new branch according to the [Rules](#rules):
   
![Name new branch](doc/how_to_start/name_new_branch.png)

3. ([Wasa](#wasa-2-player) and [Randori](#randori-3-player) only) Push the new branch to the origin repository:
   
![Push new branch](doc/how_to_start/push_new_branch.png)

## Joining a session

1. Clone this project and open it in your IDE
2. Search for the session branch you want to join and checkout it out.

![Checkout session](doc/how_to_start/chechout_session.png)

---

## Rules

### Kata (1 Player)

#### Branch Naming
When [creating a new session](#creating-a-new-session) use following naming for your session branch:

> session/*kata*/*username*

#### Game

1. Navigate to the `Kata.cs` file in the `Kata` project and implement your solution.
2. If you think you are done navigate to the `FinalTests.cs` file in the `Kata.Tests` project and run all unit tests.
3. If all unit tests pass your solution is correct otherwise go back to step 1.

Optional: You can use the `Kata.TDD` to practice test driven development (TDD)

### Wasa (2 Player)

#### Branch Naming
When [creating a new session](#creating-a-new-session) use following naming for your session branch:

> session/*wasa*/*username1*-*username2*

Example session for user *Meex* and *Monmon*: **session/wasa/Meex-Monmon**

**OR**

> session/*wasa*/*groupname*

Example session for user group *M&M*: **session/wasa/M&M**

#### Game

1. One player (P1) start by writing unit tests till one fails.
2. They (P1) commit and push their unit test(s).
3. The other player (P2) updates their local local branch with the new unit test(s).
4. They (P2) implement a solution that lets the unit test(s) pass.
5. The players switch roles (P2 writes unit tests and P1 implements a solution).

Repeat this cycle till both think they have a sufficient solution.
Then navigate to the `FinalTests.cs` file in the `Kata.Tests` project and run all unit tests.
If they all pass your solution is correct. If at least one fails your solution is not correct and you have to again repeat the cycle.

### Randori (3+ Player)

#### Branch Naming
When [creating a new session](#creating-a-new-session) use following naming for your session branch:

> session/*randori*/*groupname*

Example session for user group *MeexCorp*: **session/randori/MeexCorp**

#### Game

Same as [Wasa](#wasa-2-player) only that you cycle through all players.

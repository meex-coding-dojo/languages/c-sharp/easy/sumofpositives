using System;
using System.Diagnostics.CodeAnalysis;
using FluentAssertions;
using Xunit;

namespace Kata.Tests
{
    [ExcludeFromCodeCoverage]
    public sealed class FinalTests
    {
        [Fact]
        public void Null_is_invalid_as_input_parameter()
        {
            // Arrange
            var sut = CreateSystemUnderTest();

            // Act
            Func<int> function = () => sut.SumOverAllPositive(null);

            // Assert
            function
                .Should()
                .ThrowExactly<ArgumentNullException>();
        }

        [Fact]
        public void A_empty_list_of_values_has_a_sum_of_0()
        {
            // Arrange
            var sut = CreateSystemUnderTest();

            // Act
            var result = sut.SumOverAllPositive(Array.Empty<int>());

            // Assert
            result.Should().Be(0);
        }

        [Theory]
        [InlineData(new[]{-10, 1, -2, 20, -4}, 21)]
        [InlineData(new[]{10, -1, 2, -20, 4}, 16)]
        [InlineData(new[]{-10, -1, -2, -20, -4}, 0)]
        public void Ignore_negative_values_on_summation(int[] values, int expectedSum)
        {
            // Arrange
            var sut = CreateSystemUnderTest();

            // Act
            var result = sut.SumOverAllPositive(values);

            // Assert
            result.Should().Be(expectedSum);
        }
        
        private static IKata CreateSystemUnderTest() => new Kata();
    }
}
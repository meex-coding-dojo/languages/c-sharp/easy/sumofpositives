﻿using System;

namespace Kata
{
    public interface IKata
    {
        /// <summary>
        /// Build the sum over all positive values
        /// </summary>
        /// <param name="values">a list of values</param>
        /// <returns>sum</returns>
        /// <example>
        /// <code> Input: [1, 2, 3], Output: 6 </code>
        /// <code> Input: [-1, 1, 0], Output: 1</code>
        /// <code> Input: [], Output: 0 </code>
        /// </example>
        /// <exception cref="ArgumentNullException">when the input is <c>null</c></exception>
        int SumOverAllPositive(int[] values);
    }
}